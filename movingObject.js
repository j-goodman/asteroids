window.Asteroids = window.Asteroids || {};

var MovingObject = Asteroids.MovingObject = function (pos, vel, color, radius) {
  this.pos = pos;
  this.vel = vel;
  this.color = color;
  this.radius = radius;
};

MovingObject.prototype.draw = function (ctx) {
  ctx.fillStyle = this.color;
  ctx.beginPath();

  ctx.arc(
    this.pos[0],
    this.pos[1],
    this.radius,
    0,
    2 * Math.PI,
    false
  );

  ctx.fill();
};

MovingObject.prototype.move = function () {
  this.pos[0] += this.vel[0];
  this.pos[1] += this.vel[1];
};
