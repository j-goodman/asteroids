window.Asteroids = window.Asteroids || {};
Asteroids.Util = Asteroids.Util || {};

Asteroids.Util.inherits = function (Parent, Child) {
  var Surrogate = function () {};
  Surrogate.prototype = Parent.prototype;
  Child.prototype = new Surrogate();
  Child.prototype.constructor = Child;
};

Asteroids.Util.randomVec = function (length) {
    var brake = Math.random() * length * length;
    var squareX = brake;
    var squareY = length * length - brake;
    var x = Math.sqrt(squareX);
    var y = Math.sqrt(squareY);

    return [x, y];
};
