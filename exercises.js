var sum = function () {
  var arrArgs = Array.prototype.slice.call(arguments);
  var sum = 0;
  arrArgs.forEach(function(num) {
    sum += num;
  });
  return sum;
};

Function.prototype.myBind = function (item) {
  var fn = this;
  return function () {
    return fn.apply(item);
  };
};

Function.prototype.myBindNoArgs = function (item) {
  var fn = this;
  var subfn = function (arguments) {
    return fn.apply(item);
  };
  // subfn.arguments = arguments;
  return subfn;
};

Function.prototype.myBindNoArgs = function () {
  var fn = this;
  var args = Array.prototype.slice.call(arguments);
  var subfn = function () {
    return fn.apply(args.shift(), args);
  };
  // subfn.arguments = arguments;
  return subfn;
};

var curriedAddThreeNums = function () {
  var numbers = [];
  var args = Array.prototype.slice.call(arguments);
  var firstnum = args.shift();
  var _curriedSum = function (firstnum) {
    numbers.push(firstnum);
  };
};

var curriedSum = function (numArgs) {
  var numbers = [];
  var _curriedSum = function (num) {
    numbers.push(num);
    if (numbers.length === numArgs) {
      var sum = 0;
      numbers.forEach(function(num){
        sum += num;
      });
      return sum;
    } else {
      return _curriedSum;
    }
  };
  return _curriedSum;
};

var addThreeNums = function (a) {
  return function(b) {
    return function(c) {
      return a + b + c;
    };
  };
};

Function.prototype.curry = function (numArgs) {
  var elements = [];
  var that = this;
  var _curry = function(el) {
    elements.push(el);
    if (elements.length === numArgs) {
      return that.apply(null, elements);
    } else {
      return _curry;
    }
  };
  return _curry;
};


var summer = function(a,b,c) {
  return a + b + c;
};


Function.prototype.inherits = function (Parent) {
  var child = this;
  var Surrogate = function () {};
  Surrogate.prototype = Parent.prototype;
  child.prototype = new Surrogate();
  child.prototype.constructor = child;
};























//








//
