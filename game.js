window.Asteroids = window.Asteroids || {};
window.Asteroids.Game = window.Game || function(){};

DIM_X = 500;
DIM_Y = 500;
NUM_ASTEROIDS = 12;

Asteroids.Game.prototype.addAsteroids = function () {
  this.asteroids = [];
  var asteroid = new Asteroid(randomPosition());
  asteroids.push(asteroid);
};

Asteroids.Game.prototype.randomPosition = function () {
  x = DIM_X * Math.random();
  y = DIM_Y * Math.random();
  return [x, y];
};

Asteroids.Game.prototype.draw = function (ctx) {
  clearRect();
  asteroids.forEach(function(el) {
    el.draw(ctx);
  });
};

Asteroids.Game.prototype.moveObjects = function () {
  asteroids.forEach(function(el) {
    el.move();
  });
};
