window.Asteroids = window.Asteroids || {};

Asteroids.Asteroid = function (pos) {
  Asteroids.MovingObject.call(
    this,
    pos,
    Asteroids.Util.randomVec(),
    Asteroids.Asteroid.COLOR,
    Asteroids.Asteroid.RADIUS
  );

};

Asteroids.Asteroid.COLOR = "#A07090";
Asteroids.Asteroid.RADIUS = 6;

// window.Asteroids.Util.inherits(MovingObject, Asteroid);
